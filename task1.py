# Написати функцію, яка приймає 2 аргументи – цілі числа.
# Всередині функції виконується перевірка типу. Якщо хоча б одне з них не
# int, то повертається 1, якщо обидва int, то рахується їхня сума.
# Якщо сума додатня, повертається 0, якщо від’ємна, то -1
# якщо сума 0, повернути 2

def task1(arg_1: int, arg_2: int) -> int:
    if type(arg_1) is int and type(arg_2) is int:
        if (arg_1 + arg_2) > 0:
            return 0
        elif (arg_1 + arg_2) == 0:
            return 2
        else:
            return -1
    else:
        return 1


if __name__ == "__main__":
    cases = (
        (False, 4, 1),
        (5.15, "string", 1),
        (0, 1, 0),
        (-3, 6, 0),
        (-10, 0, -1),
        (0, 0, 2),
    )
    for arg1, arg2, result in cases:
        assert task1(arg1, arg2) == result, f"Помилка! функція task1({arg1, arg2}) = {task1(arg1, arg2)}, а очікували {result}"